# CONDUIT #



### What is CONDUIT? ###

**CONDUIT** (**C**onnecting **O**mniversal **N**etwork & **D**ata **U**tility for **I**o**T**) is a platform 
that extracts experimental data from laboratory devices via Chainlink External Adapters for ML analysis. 